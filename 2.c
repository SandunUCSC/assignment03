#include <stdio.h>

int main() {
 int num;
  printf("Please enter your number: ");
  scanf("%d", &num);

 if(num == 0)
  printf("You have entered 0(zero).\nMost of people consider it as an even number but some people say that zero is not an even or odd number.");
 else if(num % 2 == 0)
  printf("It's an even number.");
 else
  printf("It's an odd number.");
 return 0;
}
